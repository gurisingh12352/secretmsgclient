import React from "react";
import "../styles/Login.css";
import { Link } from "react-router-dom";
import { useState, useEffect } from "react";

export default function Login(props) {
  const [userEmail, setemail] = useState("");
  const [userpass, setpass] = useState("");

  useEffect(() => {
    props
      .auth()
      .then((response) => {
        console.log(response);
      })
      .catch((err) => {
        console.log(err);
      });
  });

  const handleLogin = async (e) => {
    e.preventDefault();
    console.log("form submited");
    props
      .loginUser(userEmail, userpass)
      .then((response) => {
        if (response.user_id != null) {
          window.location = "/profile";
        }
      })
      .catch((error) => console.log(error));
  };
  return (
    <>
      <div className="Login-main">
        <div className="login-card">
          <form onSubmit={handleLogin}>
            <div className="input-holder">
              <p> Username</p>
              <input
                type="email"
                required
                value={userEmail}
                onChange={(e) => {
                  setemail(e.target.value);
                }}
              />
              <p>Password</p>
              <input
                type="password"
                required
                value={userpass}
                onChange={(e) => {
                  setpass(e.target.value);
                }}
              />
              <button type="submit">SignIn</button>
            </div>
          </form>

          <Link to="/registration">CreateAccount</Link>
        </div>
      </div>
    </>
  );
}
