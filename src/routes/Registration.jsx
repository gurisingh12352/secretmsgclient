import React from "react";
import { Link } from "react-router-dom";
import "../styles/Registration.css";
import { useState ,useEffect} from "react";
import DoneIcon from "@mui/icons-material/Done";
import CloseIcon from "@mui/icons-material/Close";


export default function Registration(props) {
  //registration states
  const [username, setusername] = useState("");
  const [useremail, setemail] = useState("");
  const [usernumber, setnumber] = useState("");
  const [userpassword, setpassword] = useState("");
  const [passwordmathed, setmatched] = useState(false);

  useEffect(()=>{
    props.auth()
    .then((res)=>window.Location='/profile')
    .catch((err)=>console.log(err))
  })
  const handleform = async (e) => {
    e.preventDefault();
   props.userRegister(username,useremail,usernumber,userpassword)
   .then((response)=>{
    window.location='/profile'
    console.log(response)
   })
   .catch((err)=>{
    console.log(err); 
   })
  };
  
  return (
    <>
      <div className="Registration-main">
        <div className="RCard">
          <h1>Registration</h1>
          <form onSubmit={handleform}>
            <legend>UserName</legend>
            <input type="text" value={username} onChange={(e) => setusername(e.target.value)} required  />
            <legend>email</legend>
            <input type="email" value={useremail} onChange={(e) => setemail(e.target.value)} required  />
            <legend>Number</legend>
            <input type="tel" value={usernumber} onChange={(e) => setnumber(e.target.value)}  />
            <fieldset>
              <legend>Enter Password</legend>
              <input type="password" placeholder="Password" onChange={(e) => setpassword(e.target.value)} required />
              {passwordmathed ? <DoneIcon /> : <CloseIcon />}
              <input
                type="password"
                placeholder="Confirm password"
                onChange={(e) => {
                  e.target.value == userpassword ? setmatched(true) : setmatched(false);
                }}
                required
               
              />
              {passwordmathed ? <DoneIcon /> : <CloseIcon />}
            </fieldset>
            <button type="submit" className="btn-signup">
              SignUp
            </button>
          </form>
          <Link to="/login">Back To Login</Link>
        </div>
      </div>
    </>
  );
}
