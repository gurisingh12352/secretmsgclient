import React from "react";
import "../styles/Btn.css";
export default function Btn(props) {
  return (
    <>
      <button className="Btn-com1" type={props.type}>{props.text}</button>
    </>
  );
}
