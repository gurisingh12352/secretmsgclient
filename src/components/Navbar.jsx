import React from "react";
import "../styles/Navbar.css";
import { useState } from "react";
import { Link } from "react-router-dom";
import { useEffect } from "react";
import MenuIcon from "@mui/icons-material/Menu";

export default function Navbar(props) {
  const [togglelist, settoggle] = useState(true);
  const [isLoggedin, setlogIn] = useState(false);
  useEffect(() => {
    props
      .auth()
      .then((response) =>console.log(response))
      .catch((err) => console.log(err));
  });
  const logMeOut = async () => {
    props
      .logOut()
      .then((response) =>{
        console.log(response)
        setlogIn(false)
        window.location = "/login"
      })
      .catch((err) => console.log(err));
  };

  return (
    <>
      <div className="Nav-main">
        <h1>Secret(msg)</h1>
        {togglelist ? (
          <ul>
            <li>
              <Link aria-current="page" to="/">
                Home
              </Link>
            </li>

            {isLoggedin && (
              <li>
                <Link to="/profile">Profile</Link>
              </li>
            )}

            <li>
              <Link to="/about">About</Link>
            </li>
            <li>
              <Link to="/help">Help</Link>
            </li>
            <li>
              {isLoggedin ? (
                <Link to="/login" onClick={logMeOut}>
                  logout
                </Link>
              ) : (
                <Link to="/login">login</Link>
              )}
            </li>
          </ul>
        ) : (
          <></>
        )}

        <MenuIcon className="Nav-burger-icon" onClick={() => settoggle(!togglelist)} />
      </div>
    </>
  );
}
